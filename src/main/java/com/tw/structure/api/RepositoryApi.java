package com.tw.structure.api;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

@RestController
public class RepositoryApi {

    @RequestMapping(value = "/initRepository")
    public ResponseEntity<?> init(HttpServletRequest request) {
        RestTemplate restTemplate = new RestTemplate();
        Map<String, String> map = new HashMap<>();
        HttpHeaders headers = new HttpHeaders();
        headers.add("PRIVATE-TOKEN", "sh6ijsbPPyVjeJehLpaT");
        map.put("name", request.getParameter("name"));
        HttpEntity httpEntity = new HttpEntity(map, headers);
        Object object = restTemplate.postForObject("https://gitlab.com/api/v3/projects", httpEntity, Object.class);
        System.out.println(object.toString());
        //curl --header "PRIVATE-TOKEN: sh6ijsbPPyVjeJehLpaT" -X POST "https://gitlab.com/api/v3/projects?name=${projectName}"
//        WebClient webClient = WebClient.create();
//        Mono<String> stringMono = webClient.post().uri("https://gitlab.com/api/v3/projects").attribute("name", request.getAttribute("name"))
//                .header("PRIVATE-TOKEN", "sh6ijsbPPyVjeJehLpaT").retrieve().bodyToMono(String.class);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/packing", method = {RequestMethod.POST})
    public ResponseEntity<?> packing(@RequestBody HashMap<String, String> map) throws IOException, InterruptedException {
        String storageDir = "/Users/twer/workspaces/idea/projects/generated_project/";
        String[] commands = {"/bin/sh", "-c", "cd " + storageDir + map.get("name") + ";ls -l; ./gradlew build"};
        Process process = Runtime.getRuntime().exec(commands);
        InputStream is = process.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
        process.waitFor();
        is.close();
        reader.close();
        process.destroy();
        if (0 == process.exitValue()) {
            String libsDirPath = storageDir + map.get("name") + "/build/libs";
            File libsDir = new File(libsDirPath);
            File[] files = libsDir.listFiles();
            for (File lib : files) {
                if (lib.getAbsolutePath().endsWith(".jar")) {
                    String uploadFileToNexus = String.format("mvn deploy:deploy-file -Durl=http://localhost:8081/repository/scaleRepository/ -DrepositoryId=scaleRepository -DgroupId=com.tw -DartifactId=%s -Dversion=%s  -Dpackaging=jar -Dfile=%s", map.get("name"), System.currentTimeMillis(), lib.getAbsolutePath());
                    String[] commands_ = {"/bin/sh", "-c", uploadFileToNexus};
                    process = Runtime.getRuntime().exec(commands_);

                    InputStream is_ = process.getInputStream();
                    BufferedReader reader_ = new BufferedReader(new InputStreamReader(is_));
                    String line_;
                    while ((line_ = reader_.readLine()) != null) {
                        System.out.println(line_);
                    }
                    process.waitFor();
                    is_.close();
                    reader_.close();
                    process.destroy();
                }
            }
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }


}
