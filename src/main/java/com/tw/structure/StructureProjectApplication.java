package com.tw.structure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StructureProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(StructureProjectApplication.class, args);
	}
}
